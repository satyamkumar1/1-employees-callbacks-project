/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

const data = {
    "employees": [
        {
            "id": 23,
            "name": "Daphny",
            "company": "Scooby Doo"
        },
        {
            "id": 73,
            "name": "Buttercup",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 93,
            "name": "Blossom",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 13,
            "name": "Fred",
            "company": "Scooby Doo"
        },
        {
            "id": 89,
            "name": "Welma",
            "company": "Scooby Doo"
        },
        {
            "id": 92,
            "name": "Charles Xavier",
            "company": "X-Men"
        },
        {
            "id": 94,
            "name": "Bubbles",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 2,
            "name": "Xyclops",
            "company": "X-Men"
        }
    ]
}

const fs = require('fs');


fs.writeFile('data.json', JSON.stringify(data), (err) => {
    if (err) {
        console.log(err);
    }
    else {
        //console.log(data);
        retrieveDataOfEmployess();
    }
})





// 1. Retrieve data for ids : [2, 13, 23].  

function retrieveDataOfEmployess() {

    fs.readFile('data.json', 'utf-8', (err, data) => {
        if (err) {
            console.log(err);
        }
        else {
            // console.log(data);
            const searchData = [2, 13, 23];
            data = JSON.parse(data);

            const employeeData = Object.values(data)[0].filter((item) => {

                // console.log(item);
                return (searchData.includes(item.id));

            });
            //console.log(employeeData);
            storeDataInJsonFile('problem1.json', JSON.stringify(employeeData), 1);
            groupDataBasedOncomapnies();
        }
    })
}




// 2. Group data based on companies.

function groupDataBasedOncomapnies() {

    fs.readFile('data.json', 'utf-8', (err, data) => {
        if (err) {
            console.log(err);
        }
        else {

            data = JSON.parse(data);
            const groupData = Object.values(data)[0].reduce((acc, item) => {

                if (acc[item.company]) {
                    acc[item.company].push(item);
                }
                else {
                    acc[item.company] = [item];
                }
                return acc;

            }, {});
            console.log(groupData);
            storeDataInJsonFile('problem2.json', JSON.stringify(groupData), 2);
            dataOfPowerpuffBrigade();
        }
    });

}


//3. Get all data for company Powerpuff Brigade


function dataOfPowerpuffBrigade() {

    fs.readFile('data.json', 'utf-8', (err, data) => {
        if (err) {
            console.log(err);
        }
        else {

            data = JSON.parse(data);
            const dataOfPowerpuff = Object.values(data)[0].reduce((acc, item) => {

                if (item.company == "Powerpuff Brigade") {
                    if (acc[item.company]) {
                        acc[item.company].push(item);
                    }
                    else {
                        acc[item.company] = [item];
                    }
                }
                return acc;

            }, {});

            //console.log(dataOfPowerpuff);

            storeDataInJsonFile('problem3.json', JSON.stringify(dataOfPowerpuff), 3);
            removeEntryOfId2();
        }
    });


}

//  4. Remove entry with id 2.

function removeEntryOfId2() {

    fs.readFile('data.json', 'utf-8', (err, data) => {
        if (err) {
            console.log(err);
        }
        else {

            data = JSON.parse(data);
            const dataWithoutId2 = Object.values(data)[0].filter((item) => {

                return item.id != 2;

            });
            console.log(dataWithoutId2);
            storeDataInJsonFile('problem4.json', JSON.stringify(dataWithoutId2), 4);
            sortDataBasedOnId();

        }
    });



}

// 5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.

function sortDataBasedOnId(){

    fs.readFile('data.json', 'utf-8', (err, data) => {
        if (err) {
            console.log(err);
        }
        else {

            data = JSON.parse(data);

            const sortedData=Object.values(data)[0].sort((prev,curr)=>{


                const comapnyName1=prev.company;
                const comapnyName2=curr.company;

                if(comapnyName1>comapnyName2)
                {
                    return 1;
                }
                else if(comapnyName1<comapnyName2)
                {
                    return -1;
                }
                else{

                    const id1=prev.id;
                    const id2=curr.id;

                    if(id1>id2){
                        return 1;
                    }
                    else if(id1<id2){
                        return -1;
                    }
                    else{
                        return 0;
                    }

                }


            });

            console.log(sortedData);
            storeDataInJsonFile('problem5.json',JSON.stringify(sortedData),5);

        }
    });




}






function storeDataInJsonFile(filePath, data, id) {

    fs.writeFile(filePath, data, (err) => {
        if (err) {
            console.log(err);
        }
        else {
            console.log("data write successful" + id);
        }
    })



}